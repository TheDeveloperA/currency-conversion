﻿using Newtonsoft.Json;
using Pharmacy2U_Tech_Test.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Pharmacy2U_Tech_Test.services
{
    public class CurrencyService : ICurrency
    {
        
        public Dictionary<string, Currency> _dictionary { get; set; }

        public CurrencyService()
        {

            /* Populate dictionary with supported currencies.
             * A more efficient approach could be created but
             * as only 3 other currencies are needed there is no need.
             */

            _dictionary = new Dictionary<string, Currency>();

            populateDictionary();
          
        }

        public void populateDictionary()
        {
            using (StreamReader r = new StreamReader("./wwwroot/res/currencies.json"))
            {
                string json = r.ReadToEnd();

                IEnumerable<Currency> list = JsonConvert.DeserializeObject<IEnumerable<Currency>>(json);

                foreach (var item in list.Skip(1))
                {
                    _dictionary.Add(item.CurrencyCode, item);
                }

            }
        }
        public Dictionary<string,Currency> GetSupportedCurrency()
        {
            return _dictionary;
        }

        public Dictionary<string,Currency> ConvertCurrencyFromPounds(Dictionary<string,Currency> dictionary,string code, decimal amount)
        {
            //Calculate value first.
            var exchangeRate = Math.Round(dictionary[code].CurrencyRate,2);
           
            var value = amount * exchangeRate;


            //Add ConvertedCurrency
            var convertedCurr = new ConvertedCurrency();
            convertedCurr.CurrencyNameFrom = "GBP";
            convertedCurr.CurrencyNameTo = code;
            convertedCurr.CurrencyAmountFrom = amount;
            convertedCurr.CurrencyAmountTo = value;
            convertedCurr.date = DateTime.Now.ToString("dd/MM/yyyy");

            //Add to Dictionary
            dictionary[code].ConvertedCurrency = convertedCurr;

            return dictionary;
        }

        public Dictionary<string,Currency> ConvertCurrencyToPounds(Dictionary<string,Currency> dictionary, string codeFrom, decimal amount)
        {

            //Calculate value first.
            var exchangeRate = dictionary[codeFrom].CurrencyRate;
            // To find the inverse of the rate, it must be divided by 1 to get exchange rate of pound.
            var poundExchangeRate = Math.Round((1 / exchangeRate),2);

            var value = amount * poundExchangeRate;


            //Add ConvertedCurrency
            var convertedCurr = new ConvertedCurrency();
            convertedCurr.CurrencyNameFrom = codeFrom;
            convertedCurr.CurrencyNameTo = "GBP";
            convertedCurr.CurrencyAmountFrom = amount;
            convertedCurr.CurrencyAmountTo = value;
            convertedCurr.date = DateTime.Now.ToString("dd/MM/yyyy");

            //Add to Dictionary
            dictionary[codeFrom].ConvertedCurrency = convertedCurr;

            return dictionary;
        }

        
    }
}
