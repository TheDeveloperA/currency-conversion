Things to note:

- Exchange rates were provided from the currencies.json file, however a realistic implementation would use an API.
- All validations were done on the client-side using mainly javascript and html's in built form features.
- A dictionary was used as it would be the most efficient data structure when dealing with this scenario.
- A List was used which is a dynamic data structure, this allowed to siimply add to the list as new requests were made. However in real-time projects it would be appropriate to use a database.