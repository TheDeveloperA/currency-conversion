﻿using Pharmacy2U_Tech_Test.Models;
using System.Collections.Generic;

namespace Pharmacy2U_Tech_Test.Services
{
    public class AuditService : IAudit
    {
        public List<Audit> _audits;

        public AuditService()
        {
            _audits = new List<Audit>();
        }
        public void AddToAudit(Audit audit)
        {
            _audits.Add(audit);
        }

        public List<Audit> GetAudit()
        {
            return _audits;
        }

    }
}
