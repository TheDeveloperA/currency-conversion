﻿using System;

namespace Pharmacy2U_Tech_Test.Models
{
    public class ConvertedCurrency
    {

        public string CurrencyNameFrom { get; set; }
        public string CurrencyNameTo { get; set; }
        public decimal CurrencyAmountFrom { get; set; }
        public decimal CurrencyAmountTo { get; set; }
        public string date { get; set; }
    }
}
