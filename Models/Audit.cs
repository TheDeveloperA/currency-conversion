﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pharmacy2U_Tech_Test.Models
{
    public class Audit
    {
        public string CurrencyFrom { get; set; }
        public string CurrencyTo { get; set; }
        public decimal AmountFrom { get; set; }
        public decimal AmountTo { get; set; }
        public string TimeStamp { get; set; }

        public Audit(string currencyFrom, string currencyTo, decimal amountFrom, decimal amountTo, string date)
        {
            CurrencyFrom = currencyFrom;
            CurrencyTo = currencyTo;
            AmountFrom = amountFrom;
            AmountTo = amountTo;
            TimeStamp = date;
        }
    }
}
