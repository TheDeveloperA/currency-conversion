﻿using System.Collections.Generic;

namespace Pharmacy2U_Tech_Test.Models
{
    public interface IAudit
    {
        void AddToAudit(Audit audit);
        List<Audit> GetAudit();

    }
}
