﻿namespace Pharmacy2U_Tech_Test.Models
{
    public class Currency
    {

        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencySymbol { get; set; }
        // Due to project aims, this rate is always relative to the GBP
        public decimal CurrencyRate { get; set; }
        // Used Only when item has been submitted.
        public ConvertedCurrency ConvertedCurrency { get; set; }

        public Currency(string code, string name, string symbol,decimal rate)
        {
            CurrencyCode = code;
            CurrencyName = name;
            CurrencySymbol = symbol;
            CurrencyRate = rate;
        }
    }
}
