﻿using Pharmacy2U_Tech_Test.Models;
using System.Collections.Generic;

namespace Pharmacy2U_Tech_Test.services
{
    public interface ICurrency
    {
        Dictionary<string,Currency> ConvertCurrencyToPounds(Dictionary<string,Currency> dictionary, string code, decimal amount);
        Dictionary<string, Currency> ConvertCurrencyFromPounds(Dictionary<string,Currency> dictionary, string codeFrom, decimal amount);
        Dictionary<string,Currency> GetSupportedCurrency();
    }
}
