﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Pharmacy2U_Tech_Test.Models;
using Pharmacy2U_Tech_Test.services;

namespace Pharmacy2U_Tech_Test.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private ICurrency _currency;
        private IAudit _audit;
        public HomeController(ILogger<HomeController> logger, ICurrency currency, IAudit audit)
        {
            _logger = logger;
            _currency = currency;
            _audit = audit;
        }

        public IActionResult Index()
        {
            var model = _currency.GetSupportedCurrency();
            return View(model);
        }

        [HttpPost]
        public IActionResult Convert(string CurrencyFrom, string CurrencyTo, decimal CurrencyAmount)
        {
            string date;
            decimal amountFrom, amountTo;

            Dictionary<string, Currency> model;
            if (CurrencyFrom == "GBP")
            {
                model = _currency.ConvertCurrencyFromPounds(_currency.GetSupportedCurrency(), CurrencyTo, CurrencyAmount);
                date = model[CurrencyTo].ConvertedCurrency.date;
                amountFrom = model[CurrencyTo].ConvertedCurrency.CurrencyAmountFrom;
                amountTo = model[CurrencyTo].ConvertedCurrency.CurrencyAmountTo;
                ViewBag.Key = CurrencyTo;
            }
            else
            {
                // Getting converted to POUNDS
                model = _currency.ConvertCurrencyToPounds(_currency.GetSupportedCurrency(), CurrencyFrom, CurrencyAmount);
                date = model[CurrencyFrom].ConvertedCurrency.date;
                amountFrom = model[CurrencyFrom].ConvertedCurrency.CurrencyAmountFrom;
                amountTo = model[CurrencyFrom].ConvertedCurrency.CurrencyAmountTo;
                ViewBag.Key = CurrencyFrom;
            }

            // Add to audit logs
            var audit = new Audit(CurrencyFrom, CurrencyTo, amountFrom, amountTo, date);
            _audit.AddToAudit(audit);
            
            return View("Index",model);

        }

        public IActionResult Audit()
        {
            return View();
          
        }
        [Route("/api/logs")]
        [HttpGet]
        public IActionResult GetList()
        {
            return Json(new {data = _audit.GetAudit() });
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
